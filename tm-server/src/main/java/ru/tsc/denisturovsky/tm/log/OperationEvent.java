package ru.tsc.denisturovsky.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public final class OperationEvent {

    @NotNull
    private Object entity;

    @NotNull
    private String table;

    private long timestamp = System.currentTimeMillis();

    @NotNull
    private OperationType type;

    public OperationEvent(
            @NotNull final OperationType type,
            @NotNull final Object entity
    ) {
        this.type = type;
        this.entity = entity;
    }

}
