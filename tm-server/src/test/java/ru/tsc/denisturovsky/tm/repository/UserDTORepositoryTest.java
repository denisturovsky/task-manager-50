package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.denisturovsky.tm.api.service.IConnectionService;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.ITaskDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.repository.dto.UserDTORepository;
import ru.tsc.denisturovsky.tm.sevice.ConnectionService;
import ru.tsc.denisturovsky.tm.sevice.PropertyService;
import ru.tsc.denisturovsky.tm.sevice.dto.ProjectDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.TaskDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.UserDTOService;
import ru.tsc.denisturovsky.tm.util.HashUtil;

import javax.persistence.EntityManager;

import static ru.tsc.denisturovsky.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserDTORepositoryTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static IUserDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserDTORepository(entityManager);
    }

    @NotNull
    private static EntityManager getEntityManager() {
        return CONNECTION_SERVICE.getEntityManager();
    }

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.add(USER_TEST);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
        CONNECTION_SERVICE.close();
    }

    @Test
    public void add() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(ADMIN_TEST));
            entityManager.getTransaction().commit();
            @Nullable final UserDTO user = repository.findOneById(ADMIN_TEST.getId());
            Assert.assertNotNull(user);
            Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() throws Exception {
        @Nullable UserDTO user = USER_SERVICE.findByLogin(ADMIN_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void create() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD));
            Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
            Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createWithEmail() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD), ADMIN_TEST_EMAIL);
            Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
            Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
            Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createWithRole() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD), Role.ADMIN);
            Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
            Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
            Assert.assertEquals(Role.ADMIN, user.getRole());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId()));
        entityManager.close();
    }

    @Test
    public void findByEmail() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        @Nullable final UserDTO user = repository.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void findByLogin() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        @Nullable final UserDTO user = repository.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void findOneById() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final UserDTO user = repository.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void isEmailExists() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertTrue(repository.isEmailExists(USER_TEST.getEmail()));
        entityManager.close();
    }

    @Test
    public void isLoginExists() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertTrue(repository.isLoginExists(USER_TEST.getLogin()));
        entityManager.close();
    }

    @Test
    public void removeByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(ADMIN_TEST);
            Assert.assertNotNull(repository.findOneById(ADMIN_TEST.getId()));
            repository.remove(ADMIN_TEST);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}